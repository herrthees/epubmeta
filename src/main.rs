use epub::doc::EpubDoc;
use clap::{App, Arg};
use std::collections::HashMap;
use std::path::Path;

// Metadata keys, with their short option and help text
const METADATA_KEYS: [(&str, &str); 16] = [
    ("title", "Print the title of the book"),
    ("creator", "Print the author of the book"),
    ("identifier", "Print the identifier of the book"),
    ("language", "Print the language of the book"),
    ("contributor", "Print the contributor of the book"),
    ("date", "Print the date of the book"),
    ("subject", "Print the subject of the book"),
    ("modified", "Print the modified date of the book"),
    ("created", "Print the created date of the book"),
    ("publisher", "Print the publisher of the book"),
    ("description", "Print the description of the book"),
    ("rights", "Print the rights of the book"),
    ("source","Print the source of the book"),
    ("type", "Print the type of the book"),
    ("relation", "Print the relation of the book"),
    ("coverage", "Print the coverage of the book")
];

fn main() {
    // Setting up command-line arguments
    let mut app = App::new("epubmeta")
        .version("0.3.1")
        .author("Ralf Thees <ralf@herrthees.de>")
        .about("Extract, copy, and rename metadata for EPUB files")
        .arg(
            Arg::with_name("epubfile")
                .help("The EPUB file to process")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("action")
                .long("action")
                .help("Specify the action to perform: 'copy' or 'rename'. If not set, metadata will be printed to stdout.")
                .takes_value(true)
                .value_name("ACTION"),
        )
        .arg(
            Arg::with_name("pattern")
                .long("pattern")
                .help("The pattern  using metadata placeholders (e.g., '{creator} - {title}.epub')")
                .takes_value(true)
                .value_name("PATTERN"),
        )
        .arg(
            Arg::with_name("sanitize")
                .long("sanitize")
                .help("Sanitize the filename to remove or replace unsafe characters"),
        );

    // Add arguments for all metadata keys
    for (key, help) in METADATA_KEYS {
        app = app.arg(
            Arg::with_name(key)
                .long(key)
                .help(help)
                .takes_value(false),
        );
    }

    // Parse the command-line arguments
    let matches = app.get_matches();

    let epubfile = matches.value_of("epubfile").unwrap();
    let action = matches.value_of("action"); // Optional parameter
    let pattern = matches.value_of("pattern");
    let sanitize = matches.is_present("sanitize");

    let metadata = extract_metadata(epubfile);

    // Check if metadata keys are requested
    let filtered_metadata: HashMap<_, _> = METADATA_KEYS
        .iter()
        .filter_map(|(key, _)| {
            if matches.is_present(key) {
                Some((*key, metadata.get(*key).cloned().unwrap_or_default()))
            } else {
                None
            }
        })
        .collect();

    // If no action or pattern is given and metadata keys are specified, print this keys
    if action.is_none() && pattern.is_none() {
        if !filtered_metadata.is_empty() {
            if filtered_metadata.len() == 1 {
                // Print single metadata value
                let value = filtered_metadata.values().next().unwrap();
                println!("{}", value);
            } else {
                // Print JSON for multiple keys
                println!("{}", serde_json::to_string_pretty(&filtered_metadata).unwrap());
            }
            return;
        }

        // Default: Print all metadata if no specific keys are set
        println!("{}", serde_json::to_string_pretty(&metadata).unwrap());
        return;
    }

    // Make sure pattern is set for copy or rename actions
    let pattern = pattern.expect("A pattern must be provided with --pattern for copy or rename actions.");

    // Extract metadata from the EPUB file
    let original_path = Path::new(epubfile);
    let parent_dir = original_path.parent().unwrap_or_else(|| Path::new("."));

    // pattern the new filename
    let mut new_filename = pattern_metadata(pattern, &metadata);
    if sanitize {
        new_filename = sanitize_string(&new_filename);
    }
    let new_path = parent_dir.join(new_filename);

    // do the action copy or rename
    match action.unwrap() {
        "copy" => {
            if let Err(e) = std::fs::copy(epubfile, &new_path) {
                eprintln!("Error copying file: {}", e);
                std::process::exit(1);
            }
            println!("File copied to: {}", new_path.display());
        }
        "rename" => {
            if let Err(e) = std::fs::rename(epubfile, &new_path) {
                eprintln!("Error renaming file: {}", e);
                std::process::exit(1);
            }
            println!("File renamed to: {}", new_path.display());
        }
        _ => {
            eprintln!("Invalid action: '{}'. Use 'copy' or 'rename'.", action.unwrap());
            std::process::exit(1);
        }
    }
}

// Function to sanitize a string
fn sanitize_string(filename: &str) -> String {
    let mut sanitized = String::new();
    for c in filename.chars() {
        match c {
            '/' | '\\' | ':' | '*' | '?' | '"' | '<' | '>' | '|' | ' ' => sanitized.push('_'),
            _ => sanitized.push(c),
        }
    }
    sanitized
}

// extract metadata from the epub
fn extract_metadata(epubfile: &str) -> HashMap<String, String> {
    let mut metadata = HashMap::new();
    let epub = match EpubDoc::new(epubfile) {
        Ok(epub) => epub,
        Err(e) => {
            eprintln!("Error: {}", e);
            std::process::exit(1);
        }
    };

    // Loop through all metadata and fetch their values
    for (key, _) in METADATA_KEYS {
        if let Some(value) = epub.mdata(key) {
            metadata.insert(key.to_string(), value);
        }
    }

    metadata
}

// Replace patterns in the pattern string with metadata values
fn pattern_metadata(pattern: &str, metadata: &HashMap<String, String>) -> String {
    let mut result = pattern.to_string();
    for (key, value) in metadata {
        let placeholder = format!("{{{}}}", key); // e.g., {title}
        result = result.replace(&placeholder, value);
    }
    result
}
