
# epubmeta

A command-line tool to extract, filter, copy, and rename metadata from EPUB files. The code is written in Rust.

## Features

- **Extract Metadata**: Retrieve information such as title, author, language, publisher, and more.
- **Filter Metadata**: Display specific metadata fields using options like `--title` or `--creator`.
- **Copy EPUB Files**: Create a copy of the EPUB file with a custom filename based on metadata.
- **Rename EPUB Files**: Rename the EPUB file directly using metadata placeholders.
- **Sanitize Filenames**: Replace unsafe characters (e.g., spaces, slashes) with safe alternatives.
- **Support for Extended Metadata**: Includes fields like publisher, description, rights, and more.
- **Free and Open Source**: Licensed under the AGPL v3.

---

## Installation

### Prerequisites
Ensure you have Rust installed. If not, install it using [rustup](https://rustup.rs/).

1. **Clone the Repository**:
   ```bash
   git clone https://gitlab.com/herrthees/epubmeta
   cd epubmeta
   ```

2. **Build the Project**:
   ```bash
   cargo build --release
   ```

3. **Run the Binary**:
   ```bash
   ./target/release/epubmeta
   ```

4. **(Optional)** Add the binary to your PATH:
   ```bash
   mv ./target/release/epubmeta /usr/local/bin/
   ```

---

## Usage

### Display All Metadata
Show all available metadata for the given EPUB file:
```bash
epubmeta mybook.epub
```

**Output:**
```json
{
    "title": "The Great Gatsby",
    "creator": "F. Scott Fitzgerald",
    "publisher": "Scribner",
    "date": "1925",
    "language": "en"
}
```

### Filter Specific Metadata
Retrieve only specific metadata fields:
```bash
epubmeta mybook.epub --title --creator
```

**Output:**
```json
{
    "title": "The Great Gatsby",
    "creator": "F. Scott Fitzgerald"
}
```

### Copy the File
Create a copy of the EPUB file with a new name based on metadata:
```bash
epubmeta mybook.epub --action copy --pattern "{creator} - {title}.epub"
```

**Result:**  
```
F. Scott Fitzgerald - The Great Gatsby.epub
```

### Rename the File
Rename the original file using metadata:
```bash
epubmeta mybook.epub --action rename --pattern "{title} ({date}).epub"
```

**Result:**  
```
The Great Gatsby (1925).epub
```

### Sanitize Filenames
Replace unsafe characters in the generated filename:
```bash
epubmeta mybook.epub --action copy --pattern "{creator}:{title}.epub" --sanitize
```

**Result:**  
```
F_Scott_Fitzgerald_The_Great_Gatsby.epub
```

---

## Advanced Usage

### Bulk Processing
Process all EPUB files in a directory:
```bash
for file in *.epub; do
    epubmeta "$file" --action copy --pattern "{creator} - {title}.epub" --sanitize;
done
```

---

## Metadata Keys

The following metadata keys are supported:

| Key          | Description                   |
|--------------|-------------------------------|
| `title`      | The title of the book         |
| `creator`    | The author of the book        |
| `identifier` | Unique identifier of the book |
| `language`   | The language of the book      |
| `contributor`| Contributors to the book      |
| `date`       | The publication date          |
| `subject`    | Subject or genre of the book  |
| `modified`   | Last modification date        |
| `created`    | Creation date                 |
| `publisher`  | The publisher of the book     |
| `description`| Description or summary        |
| `rights`     | Rights and permissions        |
| `source`     | The source of the book        |
| `type`       | Type or format of the book    |
| `relation`   | Related resources             |
| `coverage`   | Coverage or scope of the book |

---

## License

`epubmeta` is licensed under the **GNU AGPL v3**.  
You are free to modify and share it, as long as you preserve the same license.

---

## AI Disclaimer

The rust code was written by a human: me. The README.md was written with the help of an AI.

---

## Contributing

Contributions are welcome!  

- **Found a bug?** Open an issue on GitLab. I wrote the code as a practice to learn Rust, so there might be some bugs - and I like to learn from them.
- **Have an idea?** Share it or submit a pull request.
- **Want to improve the code?** Me too! Fork the repo and get started!

---

## Credits

Created by **Ralf Thees**.  
Enjoy and happy reading!